import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SentimentComponent } from './src/sentiment/sentiment.component';
import { SlimScrollModule } from 'ng2-slimscroll';
export * from './src/sentiment/sentiment.component';

@NgModule({
  imports: [
    CommonModule,
    SlimScrollModule
  ],
  declarations: [
    SentimentComponent,
  ],
  exports: [
    SentimentComponent,
  ]
})
export class SentimentModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SentimentModule,
      providers: []
    };
  }
}
